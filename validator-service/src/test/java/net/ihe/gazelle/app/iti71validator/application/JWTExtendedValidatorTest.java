package net.ihe.gazelle.app.iti71validator.application;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.app.iti71validator.beans.JWTValidatorDescription;
import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;
import net.ihe.gazelle.app.iti71validator.model.JWTTokenExtended;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;

class JWTExtendedValidatorTest {

    private static final String JWT = "{" +
            "  \"iss\": \"http://issuerAdress.ch\"," +
            "  \"sub\": \"UserId-bfe8a208-b9d0-4012-b2f5-168b949fc3cb\"," +
            "  \"aud\": \"http://mhdResourceServerURL.ch\"," +
            "  \"exp\": 1587294580000," +
            "  \"nbf\": 1587294460000," +
            "  \"iat\": 1587294460000," +
            "  \"client_id\": \"client id test\"," +
            "  \"scope\": \"scope test\"," +
            "  \"jti\": \"c5436729-3f26-4dbf-abd3-2790dc7771a\"," +
            "  \"extensions\" : {" +
            "    \"ihe_iua\" : {" +
            "      \"subject_name\": \"Dagmar Musterassistent\"," +
            "      \"person_id\": \"761337610411353650^^^&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO\"," +
            "      \"subject_role\": {" +
            "          \"system\": \"urn:oid:2.16.756.5.30.1.127.3.10.6\"," +
            "          \"code\": \"HCP\"" +
            "      },\n" +
            "      \"purpose_of_use\": {" +
            "          \"system\": \"urn:uuid:2.16.756.5.30.1.127.3.10.5\"," +
            "          \"code\": \"NORM\"" +
            "      }" +
            "    }," +
            "    \"ch_epr\": {" +
            "      \"user_id\": \"2000000090092\"," +
            "      \"user_id_qualifier\": \"urn:gs1:gln\"" +
            "    }," +
            "    \"ch_group\" : [" +
            "      {" +
            "        \"name\": \"Name of group with id urn:oid:2.2.2.1\"," +
            "        \"id\": \"urn:oid:2.2.2.1\"" +
            "      }," +
            "      {" +
            "        \"name\": \"Name of group with id urn:oid:2.2.2.2\"," +
            "        \"id\": \"urn:oid:2.2.2.2\"" +
            "      }," +
            "      {" +
            "        \"name\": \"Name of group with id urn:oid:2.2.2.2\"," +
            "        \"id\": \"urn:oid:2.2.2.3\"" +
            "      }" +
            "    ]," +
            "    \"ch_delegation\": {" +
            "      \"principal\": \"Martina Musterarzt\", " +
            "      \"principal_id\": \"2000000090092\" " +
            "    }" +
            "  }" +
            "}";

    private JWTExtendedTokenValidator validator;
    private JWTValidatorDescription jwtValidatorDescription;
    private JsonNode node;
    private JWTTokenExtended jwtTokenExtended;

    @BeforeEach
    public void initializeTest() throws IOException, ParseException {
        validator = new JWTExtendedTokenValidator();
        jwtValidatorDescription = new JWTValidatorDescription(JWT);
        ObjectMapper mapper = new ObjectMapper();
        node = mapper.readValue(JWT, JsonNode.class);
        jwtTokenExtended = validator.jsonNodeToJWTExtended(node);
    }

    @Test
    void validateRequestTest() {
        validator.validateRequest(jwtValidatorDescription);
        Assertions.assertTrue(validator.isValidationPassed());
    }

    @Test
    void isValidJsonTest() {
        Assertions.assertNotNull(validator.isJsonValidFormat(JWT));
    }

    @Test
    void wellFormednessJWT() {
        Assertions.assertNotNull(validator.wellFormedNessJWT(node));
    }

    @Test
    void isValidJsonKOTest() {
        Assertions.assertNull(validator.isJsonValidFormat("[" + JWT));
    }

    @Test
    void jsonNodeToJWTExtendedTest() {
        Assertions.assertNotNull(jwtTokenExtended);
    }

    @Test
    void checkAllRequiredParametersArePresentTest() {
        validator.checkAllRequiredParametersArePresent(jwtTokenExtended);
        Assertions.assertEquals(8, validator.getNotifications().size());
    }

    @Test
    void getRequiredParametersForRequestTest() {
        Assertions.assertEquals(8, validator.getRequiredParametersForRequest().size());
    }

    @Test
    void validateParameterValueFormatTest() {
        JWTParameterValue jwtParameterValue = JWTParameterValue.ISSUER;
        String issuer = "http://issuerAdress.ch";
        validator.validateParameterValueFormat(jwtParameterValue, issuer);
        Assertions.assertEquals(1, validator.getNotifications().size());
    }
}