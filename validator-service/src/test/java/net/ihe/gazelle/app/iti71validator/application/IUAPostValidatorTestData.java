package net.ihe.gazelle.app.iti71validator.application;

import net.ihe.gazelle.app.iti71validator.beans.IUAGetURLValidatorDescription;
import net.ihe.gazelle.app.iti71validator.beans.IUAPostURLValidatorDescription;
import net.ihe.gazelle.app.iti71validator.business.IUARequestParameter;
import net.ihe.gazelle.app.iti71validator.business.ParameterType;

import java.util.ArrayList;
import java.util.List;

public class IUAPostValidatorTestData {

    private IUAPostValidatorTestData() {
    }

    static final String URL1_FULL = "http://baseurl.com/token?grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A9000%2Fcallback&code=98wrghuwuogerg97&code_verifier=qskt4342of74bkncmicdpv2qd143iqd822j41q2gupc5n3o6f1clxhpd2x11";
    static final String URL1_BASE = "http://baseurl.com/token";
    static final String URL1_PARAMETERS = "grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A9000%2Fcallback&code=98wrghuwuogerg97&code_verifier=qskt4342of74bkncmicdpv2qd143iqd822j41q2gupc5n3o6f1clxhpd2x11";
    static final String URL_FIRST_PARAMETER = "grant_type=authorization_code";
    static final String URL_SECOND_PARAMETER = "redirect_uri=http%3A%2F%2Flocalhost%3A9000%2Fcallback";
    static final String URL_THIRD_PARAMETER = "code=98wrghuwuogerg97";
    static final String URL_FOURTH_PARAMETER = "code_verifier=qskt4342of74bkncmicdpv2qd143iqd822j41q2gupc5n3o6f1clxhpd2x11";
    static final String URL_FIFTH_PARAMETER = "app-client-id";
    static final String ACCEPT_PARAMETER = "application/json";
    static final String CONTENTTYPE_PARAMETER = "application/json";
    static final String AUTHORIZATION_PARAMETER = "Basic bXktYXBwOm15LWFwcC1zZWNyZXQtMTIz";

    static final String URL1_FIRST_PARAMETER_KEY = "grant_type";
    static final String URL1_FIRST_PARAMETER_VALUE = "authorization_code";

    static final String PARAMETER_GRANTTYPE = "grant_type";
    static final String PARAMETER_CLIENTID = "client_id";
    static final String PARAMETER_REDIRECTURI = "redirect_uri";
    static final String PARAMETER_CODE = "code";
    static final String PARAMETER_CODEVERIFIER = "code_verifier";

    static final String LOCALHOST = "localhost";
    static final String URL_NO_PROTOCOL = "baseurl.com/token";
    static final String HTTP_URL = "http://" + URL_NO_PROTOCOL;
    static final String HTTPS_URL = "https://" + URL_NO_PROTOCOL;
    static final String BASE_URL_GET = "http://baseurl.com/token";

    static final IUARequestParameter PARAMETER_GRANTTYPE_DESCRIPTION = new IUARequestParameter(PARAMETER_GRANTTYPE, ParameterType.STRING, true, "authorization_code");
    static final IUARequestParameter PARAMETER_REDIRECTURI_DESCRIPTION = new IUARequestParameter(PARAMETER_REDIRECTURI, ParameterType.ID, true, null);

    static final String KO_PARAMETER = "test";

    static final IUARequestParameter KO_PARAMETER_DESCRIPTION = new IUARequestParameter(KO_PARAMETER, ParameterType.STRING, true, "ko");

    public static IUAPostURLValidatorDescription getValidatorDescriptionForPostTest() {
        return new IUAPostURLValidatorDescription(URL1_FULL, ACCEPT_PARAMETER, CONTENTTYPE_PARAMETER, AUTHORIZATION_PARAMETER);
    }

    public static List<String> getUsedParameterCompleteList(){
        List<String> parameters = new ArrayList<>();
        parameters.add(PARAMETER_GRANTTYPE);
        parameters.add(PARAMETER_REDIRECTURI);
        parameters.add(PARAMETER_CLIENTID);
        parameters.add(PARAMETER_CODE);
        parameters.add(PARAMETER_CODEVERIFIER);
        parameters.add(ACCEPT_PARAMETER);
        parameters.add(PARAMETER_REDIRECTURI);
        parameters.add(AUTHORIZATION_PARAMETER);
        return parameters;
    }

}
