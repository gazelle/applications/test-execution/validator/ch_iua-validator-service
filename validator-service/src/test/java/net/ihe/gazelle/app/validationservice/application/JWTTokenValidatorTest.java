package net.ihe.gazelle.app.validationservice.application;

import net.ihe.gazelle.sb.chiuastadardblockextended.application.CHIUAExtendedEncoderDecoder;
import net.ihe.gazelle.sb.iua.application.IUAEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.adapter.JJWTAdapter;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWSEncoderDecoder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests for {@link JWTTokenValidator}
 */
class JWTTokenValidatorTest {


    /**
     * Set the validator up
     */
    @BeforeEach
    public void setup() {

    }

    /**
     * Test default constructor.
     */
    @Test
    public void defaultConstructorTest() {
        CHIUATokenValidator impl = new CHIUATokenValidator();

        assertNotNull(impl);
    }

    /**
     * Test the validation of a token.
     */
    @Test
    public void validateTokenTest() {
        CHIUATokenValidator impl = new CHIUATokenValidator();
        IUAEncoderDecoder iuaEncoderDecoder = new IUAEncoderDecoder();
        JWSEncoderDecoder jwsEncoderDecoder = new JWSEncoderDecoder(new JJWTAdapter());
        iuaEncoderDecoder.setJwsEncoderDecoder(jwsEncoderDecoder);
        impl.setDecoder(iuaEncoderDecoder);
        String document = "azerty";

        impl.setAudienceSecretRetriever(audience -> "tarte");

        assertNotNull(impl.validateToken(document));
    }

    /**
     * Test the validation of a token including an audience.
     */
    @Test
    public void validateTokenWithAudienceTest() {
        CHIUATokenValidator impl = new CHIUATokenValidator();
        IUAEncoderDecoder iuaEncoderDecoder = new IUAEncoderDecoder();
        JWSEncoderDecoder jwsEncoderDecoder = new JWSEncoderDecoder(new JJWTAdapter());
        iuaEncoderDecoder.setJwsEncoderDecoder(jwsEncoderDecoder);
        impl.setDecoder(iuaEncoderDecoder);
        String document = "azerty\naudience";

        impl.setAudienceSecretRetriever(audience -> "tarte");

        assertNotNull(impl.validateToken(document));
    }

    /**
     * Test the validation of a token.
     */
    @Test
    public void validateTokenTestExtended() {
        CHIUAExtendedTokenValidator impl = new CHIUAExtendedTokenValidator();
        IUAEncoderDecoder iuaEncoderDecoder = new IUAEncoderDecoder();
        JWSEncoderDecoder jwsEncoderDecoder = new JWSEncoderDecoder(new JJWTAdapter());
        iuaEncoderDecoder.setJwsEncoderDecoder(jwsEncoderDecoder);
        CHIUAExtendedEncoderDecoder decoder = new CHIUAExtendedEncoderDecoder();
        decoder.setIuaEncoderDecoder(iuaEncoderDecoder);
        impl.setDecoder(decoder);
        String document = "azerty";

        impl.setAudienceSecretRetriever(audience -> "tarte");

        assertNotNull(impl.validateToken(document));
    }

    /**
     * Test the validation of a token including an audience.
     */
    @Test
    public void validateTokenWithAudienceTestExtended() {
        CHIUAExtendedTokenValidator impl = new CHIUAExtendedTokenValidator();
        IUAEncoderDecoder iuaEncoderDecoder = new IUAEncoderDecoder();
        JWSEncoderDecoder jwsEncoderDecoder = new JWSEncoderDecoder(new JJWTAdapter());
        iuaEncoderDecoder.setJwsEncoderDecoder(jwsEncoderDecoder);
        CHIUAExtendedEncoderDecoder decoder = new CHIUAExtendedEncoderDecoder();
        decoder.setIuaEncoderDecoder(iuaEncoderDecoder);
        impl.setDecoder(decoder);
        String document = "azerty\naudience";

        impl.setAudienceSecretRetriever(audience -> "tarte");

        assertNotNull(impl.validateToken(document));
    }
}
