package net.ihe.gazelle.app.validationservice.adapter.ws;

import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.app.validationservice.application.CHIUATokenValidator;
import net.ihe.gazelle.app.validationservice.application.JWTTokenValidator;
import net.ihe.gazelle.modelapi.sb.business.EncoderDecoder;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;

import java.util.Date;
import java.util.TimeZone;

/**
 * Test implementation of {@link JWTTokenValidator}
 */
public class CHIUATokenValidatorMock extends CHIUATokenValidator {

    public static final String REPORT = "<?xml version=\"1.0\" encoding=\"UTF-8\" " +
            "standalone=\"yes\"?><detailedResult><MDAValidation><Result>UNDEFINED</Result><ValidationCounters/></MDAValidation" +
            "><ValidationResultsOverview><ValidationDate>2020-08-19</ValidationDate><ValidationTime>13:15:32</ValidationTime><ValidationServiceName" +
            ">validationServiceName</ValidationServiceName><ValidationTestResult>UNDEFINED</ValidationTestResult><ValidationServiceVersion" +
            ">validationServiceVersion</ValidationServiceVersion></ValidationResultsOverview></detailedResult>";

    /**
     * {@inheritDoc}
     */
    @Override
    protected AudienceSecretRetriever getAudienceSecretRetriever() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDisclaimer() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getValidatorId() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EncoderDecoder getEncoderDecoder() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationReport validateToken(String document) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Date validationDateTime = new Date(Long.valueOf("1597842932511"));
        return new ValidationReport("UUID", new ValidationOverview("disclaimer", validationDateTime,
                "validationServiceName", "validationServiceVersion", "validatorID"));
    }
}
