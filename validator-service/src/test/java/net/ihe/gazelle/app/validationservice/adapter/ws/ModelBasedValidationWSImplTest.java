package net.ihe.gazelle.app.validationservice.adapter.ws;

import net.ihe.gazelle.app.validationservice.application.CHIUAExtendedTokenValidator;
import net.ihe.gazelle.validator.mb.ws.SOAPException_Exception;
import net.ihe.gazelle.app.validationservice.application.CHIUATokenValidator;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link ModelBasedValidationWSImpl}
 */
public class ModelBasedValidationWSImplTest {

    /**
     * Test validateBase64Document with document and validator
     *
     * @throws SOAPException_Exception
     */
    @Test
    public void validateBase64Document() throws SOAPException_Exception {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();
        ws.setCHIUATokenValidator(new CHIUATokenValidatorMock());

        String report = ws.validateBase64Document("VGFydGUgYXV4IHBvbW1lcw==", CHIUATokenValidator.VALIDATOR_ID);

        assertEquals(CHIUATokenValidatorMock.REPORT, report, "Report shall be equal to Mocked report !");
    }

    /**
     * Test validateBase64Document without validator
     */
    @Test
    public void validateBase64DocumentNoValidator() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();
        ws.setCHIUATokenValidator(new CHIUATokenValidatorMock());

        assertThrows(SOAPException_Exception.class, () -> ws.validateBase64Document("VGFydGUgYXV4IHBvbW1lcw==",
                null), "Error expected when no validator is given !");
    }

    /**
     * Test validateBase64Document without document
     */
    @Test
    public void validateBase64DocumentNoDocument() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();
        ws.setCHIUATokenValidator(new CHIUATokenValidatorMock());

        assertThrows(SOAPException_Exception.class, () -> ws.validateBase64Document("",
                CHIUATokenValidator.VALIDATOR_ID), "Error expected when no document is given !");
    }

    /**
     * Test validateDocument  with document and validator
     *
     * @throws SOAPException_Exception
     */
    @Test
    public void validateDocument() throws SOAPException_Exception {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();
        ws.setCHIUATokenValidator(new CHIUATokenValidatorMock());

        String report = ws.validateDocument("Tarte aux pommes", CHIUATokenValidator.VALIDATOR_ID);

        assertEquals(CHIUATokenValidatorMock.REPORT, report, "Report shall be equal to Mocked report !");
    }

    /**
     * Test validateDocument without validator
     */
    @Test
    public void validateDocumentNoValidator() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();
        ws.setCHIUATokenValidator(new CHIUATokenValidatorMock());

        assertThrows(SOAPException_Exception.class, () -> ws.validateDocument("Tarte aux pommes", ""),
                "Exception expected when no validator is given !");
    }

    /**
     * Test validateDocument without document
     */
    @Test
    public void validateDocumentNoDocument() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();
        ws.setCHIUATokenValidator(new CHIUATokenValidatorMock());

        assertThrows(SOAPException_Exception.class, () -> ws.validateDocument(null, CHIUATokenValidator.VALIDATOR_ID),
                "Exception expected when no document is given !");
    }

    /**
     * Test about.
     */
    @Test
    public void about() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();

        assertNotNull(ws.about(), "About disclaimer shall not be null ! ");
    }

    /**
     * Test getListOfValidators.
     */
    @Test
    public void getListOfValidators() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();

        List<String> validators = ws.getListOfValidators("not used anyway");

        assertNotNull(validators, "Validator list shall not be null !");
        assertEquals(2, validators.size(), "Validator list shall contain a single element !");
        assertEquals(CHIUATokenValidator.VALIDATOR_ID, validators.get(0), "Validator list shall contain the validator implementation ID !");
        assertEquals(CHIUAExtendedTokenValidator.VALIDATOR_ID, validators.get(1), "Validator list shall contain the extended validator " +
                "implementation ID !");
    }

    /**
     * Test getValidatorsAsString.
     */
    @Test
    public void getValidatorsAsString() {
        ModelBasedValidationWSImpl ws = new ModelBasedValidationWSImpl();

        assertThrows(UnsupportedOperationException.class, () -> ws.getValidatorsAsString(), "Method is not supported !");
    }
}