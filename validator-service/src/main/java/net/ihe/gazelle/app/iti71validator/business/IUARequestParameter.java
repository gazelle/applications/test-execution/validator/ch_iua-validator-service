package net.ihe.gazelle.app.iti71validator.business;

import net.ihe.gazelle.app.iti71validator.beans.IUAGetURLValidatorDescription;

import java.io.Serializable;
import java.util.List;

public class IUARequestParameter implements IIUARequestParameter, Serializable {
    private String name;

    private boolean required;

    private String regex;

    private ParameterType type;

    private transient IUAGetURLValidatorDescription validatorDescription;

    private List<ValueSetForParameter> allowedListsOfCodes;

    public IUARequestParameter(){
        this.required = false;
        this.type = ParameterType.STRING;
    }

    public IUARequestParameter(IUAGetURLValidatorDescription inValidatorDescription){
        this.validatorDescription = inValidatorDescription;
        this.required = false;
        this.type = ParameterType.STRING;
    }

    public IUARequestParameter(String inName, ParameterType inType, boolean isRequired, String inRegex) {
        this.name = inName;
        this.type = inType;
        this.required = isRequired;
        this.regex = inRegex;
    }

    @Override
    public ParameterType getType() {
        return type;
    }

    public void setType(ParameterType type) {
        this.type = type;
    }


    public IUAGetURLValidatorDescription getValidatorDescription() {
        return validatorDescription;
    }

    public void setValidatorDescription(IUAGetURLValidatorDescription validatorDescription) {
        this.validatorDescription = validatorDescription;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }


    @Override
    public String getRegex() {
        return regex;
    }


    public void setRegex(String regex) {
        this.regex = regex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IUARequestParameter)) {
            return false;
        }

        IUARequestParameter that = (IUARequestParameter) o;

        if (required != that.required) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return regex != null ? regex.equals(that.regex) : that.regex == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (required ? 1 : 0);
        result = 31 * result + (regex != null ? regex.hashCode() : 0);
        return result;
    }

    public List<ValueSetForParameter> getAllowedListsOfCodes() {
        return allowedListsOfCodes;
    }

    public void setAllowedListsOfCodes(List<ValueSetForParameter> allowedListsOfCodes) {
        this.allowedListsOfCodes = allowedListsOfCodes;
    }
}
