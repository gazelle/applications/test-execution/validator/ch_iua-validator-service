package net.ihe.gazelle.app.iti71validator.application;

import net.ihe.gazelle.app.iti71validator.beans.IUAPostURLValidatorDescription;
import net.ihe.gazelle.app.iti71validator.business.*;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.*;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Error;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class RequestHTTPPostURLValidator {

    IUAPostURLValidatorDescription validatorDescription;
    private List<String> usedParameters;
    private final MDAValidation validationResult;
    private int errorCount = 0;
    private int noteCount = 0;

    public RequestHTTPPostURLValidator() {
        usedParameters = new ArrayList<>();
        validationResult = new MDAValidation();
    }

    protected void addError(Notification error) {
        validationResult.getWarningOrErrorOrNote().add(error);
        errorCount++;
    }

    protected void addNote(Notification note) {
        validationResult.getWarningOrErrorOrNote().add(note);
        noteCount++;
    }

    public MDAValidation validateRequest(IUAPostURLValidatorDescription iuaPostURLValidatorDescription) {
        this.validatorDescription = iuaPostURLValidatorDescription;
        if (validatorDescription.getUrlToValidate() == null || validatorDescription.getUrlToValidate().isEmpty()) {
            Notification error = new Error();
            error.setDescription("The URL to validate is empty");
            addError(error);
        } else {
            // split using ? (separates base URL from URL parameters)
            String[] urlParts = splitStringAndGetParts(validatorDescription.getUrlToValidate(), "\\?");
            if (urlParts.length == 2) {
                validateBaseUrl(urlParts[0]);
                validateUrlParameters(urlParts[1]);
            } else if (urlParts.length == 1) {
                validateBaseUrl(urlParts[0]);
            } else {
                Notification error = new Error();
                error.setDescription("? is a reserved character");
                addError(error);
            }
        }
        addValidationCounters();
        String validationStatus = errorCount == 0 ? ReportCreator.PASSED : ReportCreator.FAILED;
        validationResult.setResult(validationStatus);
        return validationResult;
    }

    protected void addValidationCounters() {
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfValidationWarnings(BigInteger.ZERO);
        counters.setNrOfValidationInfos(BigInteger.ZERO);
        counters.setNrOfValidationNotes(BigInteger.valueOf(noteCount));
        counters.setNrOfValidationErrors(BigInteger.valueOf(errorCount));
        counters.setNrOfChecks(BigInteger.valueOf((long) noteCount + errorCount));
        validationResult.setValidationCounters(counters);
    }

    public String[] splitStringAndGetParts(String inString, String delimiter) {
        return inString.split(delimiter);
    }

    public void validateUrlParameters(String parametersFromUrl) {
        String[] parameters = splitStringAndGetParts(parametersFromUrl, "&"); // we shall not match "\&" here
        for (String parameter : parameters) {
            validateUrlParameter(parameter);
        }
        validateParameterValue(validatorDescription.getAccept(), HTTPPostHeaderParameter.ACCEPT);
        validateParameterValue(validatorDescription.getContentType(), HTTPPostHeaderParameter.CONTENTTYPE);
        validateParameterValue(validatorDescription.getAuthorization(), HTTPPostHeaderParameter.AUTHORIZATION);

        checkAllRequiredHeadersParametersArePresent();
        checkAllRequiredRequestParametersArePresent();
    }

    public void checkAllRequiredHeadersParametersArePresent() {
        List<HTTPPostHeaderParameter> requiredParameters = getRequiredParametersForRequest();
        for (HTTPPostHeaderParameter requiredParam : requiredParameters) {
            final String name = requiredParam.getName();
            if (usedParameters.contains(name)) {
                Notification note = new Note();
                note.setDescription(requiredParam.getName() + " is required and present");
                addNote(note);
            } else {
                Notification error = new Error();
                error.setDescription("A parameter named " + requiredParam.getName() + " is required but missing in this URL");
                addError(error);
            }
        }
    }

    private List<HTTPPostHeaderParameter> getRequiredParametersForRequest() {
        List<HTTPPostHeaderParameter> allParameters = validatorDescription.getHeaderParameters();
        List<HTTPPostHeaderParameter> requiredParameters = new ArrayList<>();
        for (HTTPPostHeaderParameter parameter : allParameters) {
            if (parameter.isRequired()) {
                requiredParameters.add(parameter);
            }
        }
        return requiredParameters;
    }

    public void checkAllRequiredRequestParametersArePresent() {
        List<HTTPPostRequestParameter> requiredParameters = getRequiredScopeParametersForRequest();
        for (HTTPPostRequestParameter requiredParam : requiredParameters) {
            final String name = requiredParam.getName();
            if (usedParameters.contains(name)) {
                Notification note = new Note();
                note.setDescription(requiredParam.getName() + " is required and present");
                addNote(note);
            } else {
                Notification error = new Error();
                error.setDescription("A parameter named " + requiredParam.getName() + " is required but missing in this URL");
                addError(error);
            }
        }
    }

    private List<HTTPPostRequestParameter> getRequiredScopeParametersForRequest() {
        List<HTTPPostRequestParameter> allParameters = validatorDescription.getRequestParameters();
        List<HTTPPostRequestParameter> requiredParameters = new ArrayList<>();
        for (HTTPPostRequestParameter parameter : allParameters) {
            if (parameter.isRequired()) {
                requiredParameters.add(parameter);
            }
        }
        return requiredParameters;
    }

    public void validateUrlParameter(String parameter) {
        String[] parameterParts = splitStringAndGetParts(parameter, "=");
        if (parameterParts.length != 2) {
            Notification error = new Error();
            error.setDescription("A parameter shall be made of a couple name/value");
            error.setLocation(parameter);
            addError(error);
        } else {
            IIUARequestParameter testedParameter = validateParameterKey(parameterParts[0]);
            if (testedParameter != null) {
                if (parameterParts[1].contains("callback")) {
                    try {
                        validateParameterValue(URLDecoder.decode(parameterParts[1], StandardCharsets.UTF_8.toString()), testedParameter);
                    } catch (UnsupportedEncodingException e) {
                        Notification error = new Error();
                        error.setDescription("A parameter shall be URL encoded");
                        error.setLocation(testedParameter.getName());
                        addError(error);
                    }
                } else {
                    validateParameterValue(parameterParts[1], testedParameter);
                }
            }
        }
    }

    public IIUARequestParameter validateParameterKey(String key) {
        IIUARequestParameter parameterDefinition = getParameterIfAllowed(key);
        if (parameterDefinition != null) {
            return parameterDefinition;
        } else {
            Notification error = new Error();
            error.setDescription(key + " is not allowed for parameters");
            error.setLocation(key);
            addError(error);
            return null;
        }
    }

    public IIUARequestParameter getParameterIfAllowed(String keyName) {
        List<HTTPPostRequestParameter> allowedParameters = validatorDescription.getRequestParameters();
        IIUARequestParameter foundParameter = HTTPPostRequestParameter.getParameterByName(keyName);
        if (foundParameter == null) {
            for (HTTPPostRequestParameter parameter : allowedParameters) {
                if (parameter.getName().equals(keyName)) {
                    foundParameter = parameter;
                }
            }
        }
        if (foundParameter != null) {
            Notification note = new Note();
            note.setDescription(keyName + " parameter is present");
            addNote(note);
            return foundParameter;
        } else {
            Notification error = new Error();
            error.setDescription(keyName + " is not an allowed parameter");
            addError(error);
            return null;
        }
    }

    protected void validateParameterValue(String parameterPart, IIUARequestParameter testedParameter) {
        // first split the repetitions (used for OR clauses)
        String[] repetitions = splitStringAndGetParts(parameterPart, ","); // we shall not match "\," here
        String key = null;
        for (String value : repetitions) {
            key = testedParameter.getName();
            validateParameterValueFormat(value, testedParameter);
        }
        usedParameters.add(key);
    }

    public void validateParameterValueFormat(String value, IIUARequestParameter testedParameter) {
        String regex = testedParameter.getRegex();
        if (regex == null || regex.isEmpty()) {
            regex = testedParameter.getType().getDefaultRegex();
        }
        final Pattern pattern = Pattern.compile(regex);
        if (pattern.matcher(value).matches()) {
            Notification note = new Note();
            note.setDescription("Value for parameter " + testedParameter.getName() + " is correctly formatted");
            note.setLocation(value);
            addNote(note);
        } else {
            Notification error = new Error();
            error.setDescription("Value for parameter " + testedParameter.getName() + " shall match regex " + regex);
            error.setLocation(value);
            addError(error);
        }
    }

    public void validateBaseUrl(String baseUrl) {
        // check protocol and start parsing after protocol declaration
        String baseUrlNoProtocol = checkProtocolAndReturnUrl(baseUrl);
        if (baseUrlNoProtocol != null) {
            // check URL format
            String[] parts = baseUrlNoProtocol.split("/");
            int index = 0;
            boolean resourceIsPresent = false;
            for (String part : parts) {
                resourceIsPresent = validateBaseUrlParts(part, index);
                if (resourceIsPresent) {
                    break;
                }
                index++;
            }
            if (!resourceIsPresent) {
                Notification error = new Error();
                error.setDescription("Declaration of the resource is missing, expecting " + validatorDescription.getResourceName());
                addError(error);
            }
        }
    }

    /**
     * @return true if we reach the resourceName part of the URL
     */
    public boolean validateBaseUrlParts(String part, int index) {
        if (index == 0) {
            validateDomain(part);
        } else if (part.equals(validatorDescription.getResourceName())) {
            Notification note = new Note();
            note.setDescription("Query is performed on resource with name: " + part);
            addNote(note);
            return true;
        } else {
            Pattern pattern = Pattern.compile(ValidatorConstants.URL_PART_PATTERN);
            if (!pattern.matcher(part).matches()) {
                Notification error = new Error();
                error.setDescription("The URL is not valid");
                error.setLocation(part);
                addError(error);
            }
        }
        return false;
    }

    public void validateDomain(String part) {
        Pattern domainPattern = Pattern.compile(ValidatorConstants.DOMAIN);
        Notification notification;
        if (domainPattern.matcher(part).matches()) {
            notification = new Note();
            notification.setDescription("A valid domain is used");
            notification.setLocation(part);
            addNote(notification);
        } else {
            notification = new Error();
            notification.setDescription("This is not a valid domain name");
            notification.setLocation(part);
            addError(notification);
        }
    }

    public String checkProtocolAndReturnUrl(String baseUrl) {
        String baseUrlNoProtocol = null;
        if (baseUrl.startsWith(ValidatorConstants.HTTP)) {
            baseUrlNoProtocol = baseUrl.substring(ValidatorConstants.HTTP.length());
            Notification note = new Note();
            note.setDescription("HTTP protocol is used");
            addNote(note);
        } else if (baseUrl.startsWith(ValidatorConstants.HTTPS)) {
            baseUrlNoProtocol = baseUrl.substring(ValidatorConstants.HTTPS.length());
            Notification note = new Note();
            note.setDescription("HTTPS protocol is used");
            addNote(note);
        } else {
            Notification error = new Error();
            error.setDescription("Either HTTP or HTTPS protocols shall be used");
            error.setLocation(baseUrl);
            addError(error);
        }
        return baseUrlNoProtocol;
    }

    public List<Object> getNotifications() {
        return validationResult.getWarningOrErrorOrNote();
    }

    public void setUsedParameters(List<String> usedParameters) {
        this.usedParameters = usedParameters;
    }

    public void setValidatorDescription(IUAPostURLValidatorDescription validatorDescription) {
        this.validatorDescription = validatorDescription;
    }

    public IUAPostURLValidatorDescription getValidatorDescription() {
        return validatorDescription;
    }

    public boolean isValidationPassed() {
        return errorCount == 0;
    }
}
