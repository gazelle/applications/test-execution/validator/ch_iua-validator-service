package net.ihe.gazelle.app.validationservice.application;

import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.modelapi.sb.business.DecodingException;
import net.ihe.gazelle.modelapi.sb.business.EncoderDecoder;
import net.ihe.gazelle.modelapi.sb.business.ValidationException;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.EncodedJSONWebToken;
import net.ihe.version.ws.Interface.VersionProvider;

import java.util.Date;
import java.util.UUID;

/**
 * validator for JWT
 */
public abstract class JWTTokenValidator {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(JWTTokenValidator.class);
    private static final String VALIDATION_SERVICE_NAME = "CH:IUA Validation Service";

    /**
     * Return the {@link AudienceSecretRetriever} to use
     *
     * @return a {@link AudienceSecretRetriever}
     */
    protected abstract AudienceSecretRetriever getAudienceSecretRetriever();

    /**
     * Return the content of the disclaimer.
     *
     * @return literal content of the disclaimer.
     */
    protected abstract String getDisclaimer();

    /**
     * Return the Validator Identifier.
     *
     * @return literal value of the identifier.
     */
    protected abstract String getValidatorId();

    /**
     * Return the EncoderDecoder to use for validation.
     *
     * @return the {@link EncoderDecoder}
     */
    protected abstract EncoderDecoder getEncoderDecoder();

    /**
     * Validate token.
     *
     * @param document full document sent to the MBV WS.
     * @return {@link ValidationReport} summing up performed validation.
     */
    public ValidationReport validateToken(String document) {
        EncodedJSONWebToken token = new EncodedJSONWebToken(UUID.randomUUID().toString(), "rfc7519", parseDocumentForToken(document),
                getAudienceSecretRetriever().retrieveSecretForAudience(parseDocumentForAudience(document)));
        ValidationReport validationReport = new ValidationReport(UUID.randomUUID().toString(),
                new ValidationOverview(getDisclaimer(), new Date(), VALIDATION_SERVICE_NAME,
                        VersionProvider.get(), getValidatorId()));
        try {
            getEncoderDecoder().decodeAndReportValidation(token, validationReport);
        } catch (DecodingException | ValidationException ignored) {
            LOGGER.info("Ignored exception : ", ignored);
        }
        return validationReport;
    }

    /**
     * Parse input document and return the actual token value.
     *
     * @param document full document sent to the MBV WS.
     * @return literal value of the token.
     */
    private String parseDocumentForToken(String document) {
        return document.split("\n")[0];
    }

    /**
     * Parse input document and return the actual Audience value.
     *
     * @param document full document sent to the MBV WS.
     * @return literal value of the Audience.
     */
    private String parseDocumentForAudience(String document) {
        String[] splitDocument = document.split("\n");
        return splitDocument.length == 2 ? splitDocument[1] : null;
    }
}
