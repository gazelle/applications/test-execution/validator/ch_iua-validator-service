package net.ihe.gazelle.app.iti71validator.business;

import java.util.Arrays;
import java.util.List;

public enum JWTParameterValue {
    ISSUER("iss", ParameterType.URI, true, null),
    SUBJECT("sub", ParameterType.STRING, true, null),
    AUDIENCE("aud", ParameterType.URI, true, null),
    EXPIRATION("exp", ParameterType.NUMBER, true, null),
    NOTBEFORE("nbf", ParameterType.NUMBER,  false, null),
    ISSUEDAT("iat", ParameterType.NUMBER, true, null),
    CLIENTID("client_id", ParameterType.STRING, true, null),
    SCOPE("scope", ParameterType.STRING, true, null),
    JWTID("jti", ParameterType.STRING, true, null),

    SUBJECTNAME("subject_name", ParameterType.STRING, false, null),
    PERSONID("person_id", ParameterType.STRING, false, "^.+?\\^\\^\\^&amp;.+?&amp;ISO$"),

    SUBJECTROLESYSTEM("system", ParameterType.NUMBER, false, "^urn:oid:2.16.756.5.30.1.127.3.10.6$"),
    SUBJECTROLECODE("code", ParameterType.NUMBER, false, "HCP|ASS|REP|PAT"),

    PURPOSEOFUSESYSTEM("system", ParameterType.NUMBER, false, "^urn:uuid:2.16.756.5.30.1.127.3.10.5$"),
    PURPOSEOFUSECODE("code", ParameterType.NUMBER, false, "NORM|EMER"),

    USERID("user_id", ParameterType.NUMBER, false, null),
    USERIDQUALIFIER("user_id_qualifier", ParameterType.STRING, false, null),

    CHGROUPNAME("name", ParameterType.STRING, false, null),
    CHGROUPID("id", ParameterType.OID, false, null),

    PRINCIPAL("principal", ParameterType.STRING, false, null),
    PRINCIPALID("principal_id", ParameterType.NUMBER, false, null);

    final String key;
    final ParameterType inType;
    boolean required;

    final String regex;

    JWTParameterValue(String key, ParameterType inType, boolean required, String regex) {
        this.key = key;
        this.inType = inType;
        this.required = required;
        this.regex = regex;
    }

    public String getKey() { return this.key; }

    public ParameterType getType() {
        return this.inType;
    }

    public boolean isRequired() {
        return this.required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getRegex() {
        return regex;
    }

    public static JWTParameterValue getParameterByKey(String inKey){
        for (JWTParameterValue parameter: values()){
            if (parameter.getKey().equals(inKey)){
                return parameter;
            }
        }
        return null;
    }

    public static List<JWTParameterValue> getAllParameters() {
        return Arrays.asList(values());
    }
}
