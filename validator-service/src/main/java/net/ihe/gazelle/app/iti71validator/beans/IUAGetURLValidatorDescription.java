package net.ihe.gazelle.app.iti71validator.beans;

import net.ihe.gazelle.app.iti71validator.business.HTTPGetRequestParameter;
import net.ihe.gazelle.app.iti71validator.business.ScopeParameter;

import java.util.List;
import java.util.Objects;

//TODO à remanier lors du refacto
public class IUAGetURLValidatorDescription {

    private List<HTTPGetRequestParameter> requestParameters;

    private List<ScopeParameter> scopeParameters;

    private final boolean isExtended;

    private final String urlToValidate;

    public IUAGetURLValidatorDescription(boolean isExtended, String urlToValidate) {
        this.requestParameters = HTTPGetRequestParameter.getAllParameters();
        this.scopeParameters = ScopeParameter.getAllParameters();
        this.isExtended = isExtended;
        this.urlToValidate = urlToValidate;
    }

    public List<HTTPGetRequestParameter> getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(List<HTTPGetRequestParameter> requestParameters) {
        this.requestParameters = requestParameters;
    }

    public List<ScopeParameter> getScopeParameters() {
        return scopeParameters;
    }

    public void setScopeParameters(List<ScopeParameter> scopeParameters) {
        this.scopeParameters = scopeParameters;
    }

    public boolean isExtended() {
        return isExtended;
    }

    public String getResourceName() {
        return "authorize";
    }

    public String getUrlToValidate() {
        return urlToValidate;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IUAGetURLValidatorDescription)) {
            return false;
        }

        IUAGetURLValidatorDescription that = (IUAGetURLValidatorDescription) o;
        return Objects.equals(requestParameters, that.requestParameters) && Objects.equals(scopeParameters, that.scopeParameters);
    }

    public int hashCode() {
        int result = requestParameters != null ? requestParameters.hashCode() : 0;
        result = 31 * result + (scopeParameters != null ? scopeParameters.hashCode() : 0);
        return result;
    }
}
