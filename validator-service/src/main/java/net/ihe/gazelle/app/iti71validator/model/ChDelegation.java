package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class ChDelegation {

    private String principal;
    private String principalId;

    public ChDelegation(String principal, String principalId) {
        this.principal = principal;
        this.principalId = principalId;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(String principalId) {
        this.principalId = principalId;
    }

    public Map<JWTParameterValue, String> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, String> allParam = new TreeMap<>();
        if (this.principal != null && this.principalId != null) {
            allParam.put(JWTParameterValue.PRINCIPAL, principal);
            allParam.put(JWTParameterValue.PRINCIPALID, principalId);
            return allParam;
        }
        return Collections.emptyMap();
    }
}
