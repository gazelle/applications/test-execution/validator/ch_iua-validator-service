package net.ihe.gazelle.app.iti71validator.business;

import java.io.Serializable;

public class ValueSetForParameter implements Serializable {

    private String namespaceURI;

    private String valueSetOid;

    private boolean extensible;

    private IUARequestParameter requestParameter;

    public ValueSetForParameter() {
        //Empty constructor
    }

    public String getNamespaceURI() {
        return namespaceURI;
    }

    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }

    public String getValueSetOid() {
        return valueSetOid;
    }

    public void setValueSetOid(String valueSetOid) {
        this.valueSetOid = valueSetOid;
    }

    public boolean isExtensible() {
        return extensible;
    }

    public void setExtensible(boolean extensible) {
        this.extensible = extensible;
    }

    public IUARequestParameter getRequestParameter() {
        return requestParameter;
    }

    public void setRequestParameter(IUARequestParameter requestParameter) {
        this.requestParameter = requestParameter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ValueSetForParameter)) {
            return false;
        }

        ValueSetForParameter that = (ValueSetForParameter) o;

        if (extensible != that.extensible) {
            return false;
        }
        if (namespaceURI != null ? !namespaceURI.equals(that.namespaceURI) : that.namespaceURI != null) {
            return false;
        }
        return valueSetOid != null ? valueSetOid.equals(that.valueSetOid) : that.valueSetOid == null;
    }

    @Override
    public int hashCode() {
        int result = namespaceURI != null ? namespaceURI.hashCode() : 0;
        result = 31 * result + (valueSetOid != null ? valueSetOid.hashCode() : 0);
        result = 31 * result + (extensible ? 1 : 0);
        return result;
    }
}
