package net.ihe.gazelle.app.iti71validator.business;

public enum ParameterModifier {

    EXACT("exact"),
    CONTAINS("contains"),
    TEXT("text"),
    NOT("not"),
    ABOVE("above"),
    BELOW("below"),
    IN("in"),
    NOTIN("not-in"),
    MISSING("missing"), // parameter value for this name is restricted to true|false
    TYPE("[type]"); // type stands for the datatype of the resource used as reference

    ParameterModifier(String inModifier){
        this.name = inModifier;
    }

    String name;

    public static ParameterModifier getModifierByName(String usedModifier) {
        if (usedModifier.startsWith("[") && usedModifier.endsWith("]")){
            return TYPE;
        } else {
            for (ParameterModifier modifier : values()) {

                if (modifier.getName().equals(usedModifier)) {
                    return modifier;
                }
            }
            return null;
        }
    }

    public String getName() {
        return name;
    }
}