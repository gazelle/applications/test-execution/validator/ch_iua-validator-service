package net.ihe.gazelle.app.validationservice.adapter.preferences;

import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CHIUAVSOperationalPreferences implements OperationalPreferencesClientApplication {

    /**
     * Default constructor for the class.
     */
    public CHIUAVSOperationalPreferences() {
        //Emtpy constructor for injection
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        return new HashMap<>();
    }
}
