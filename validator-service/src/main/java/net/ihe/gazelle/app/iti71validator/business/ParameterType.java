package net.ihe.gazelle.app.iti71validator.business;

import java.util.Arrays;
import java.util.List;

public enum ParameterType {

    // simple numerical value (negative values and float values accepted)
    NUMBER("number", ValidatorConstants.NUMBER_REGEX, true, ParameterModifier.MISSING),
    // The dates parameter format is yyyy-mm-ddThh:mm:ss[Z|(+|-)hh:mm] (the standard XML format).
    DATE("date", ValidatorConstants.DATE_TIME_REGEX, true, ParameterModifier.MISSING),
    STRING("string", ValidatorConstants.STRING_REGEX, false, ParameterModifier.MISSING, ParameterModifier.EXACT, ParameterModifier.CONTAINS),
    // [code] or |[code] or [system]|[code] or [system]| where system is formatted as an URI
    TOKEN("token", ValidatorConstants.TOKEN_REGEX, false, ParameterModifier.MISSING, ParameterModifier.TEXT, ParameterModifier.IN, ParameterModifier.BELOW, ParameterModifier.ABOVE, ParameterModifier.NOTIN),
    REFERENCE("reference", "", false, ParameterModifier.MISSING, ParameterModifier.TYPE),
    QUANTITY("quantity", ValidatorConstants.QUANTITY_REGEX, true, ParameterModifier.MISSING),
    URI("uri", ValidatorConstants.URI_REGEX, false, ParameterModifier.BELOW, ParameterModifier.ABOVE, ParameterModifier.MISSING),
    ID("id", ValidatorConstants.ID_REGEX, false, ParameterModifier.MISSING),
    OID("oid", ValidatorConstants.OID_REGEX, false, ParameterModifier.MISSING),
    AUTHORIZATION("Authorization", ValidatorConstants.AUTHORIZATION, false, ParameterModifier.MISSING);


    ParameterType(String inName, String inDefaultRegex, boolean supportsPrefix, ParameterModifier... inModifiers){
        this.name = inName;
        this.defaultRegex = inDefaultRegex;
        this.allowedModifiers = inModifiers;
        this.supportsPrefix = supportsPrefix;
    }

    String name;
    String defaultRegex;
    ParameterModifier[] allowedModifiers;
    boolean supportsPrefix;

    public String getName() {
        return name;
    }

    public String getDefaultRegex() {
        return defaultRegex;
    }

    public ParameterModifier[] getAllowedModifiers() {
        return allowedModifiers;
    }

    public boolean allowsModifier(ParameterModifier parameterModifier) {
        List<ParameterModifier> modifiers = Arrays.asList(allowedModifiers);
        return modifiers.contains(parameterModifier);
    }

    public boolean isSupportsPrefix() {
        return supportsPrefix;
    }
}
