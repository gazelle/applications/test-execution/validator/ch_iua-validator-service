package net.ihe.gazelle.app.iti71validator.business;

public interface IIUARequestParameter {
    String getName();

    ParameterType getType();

    boolean isRequired();

    String getRegex();
}
