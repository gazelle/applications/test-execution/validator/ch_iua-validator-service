package net.ihe.gazelle.app.iti71validator.business;

import java.util.Arrays;
import java.util.List;

public enum HTTPPostRequestParameter implements IIUARequestParameter {

    CLIENTID("client_id", ParameterType.ID, true, null),
    REDIRECTURI("redirect_uri", ParameterType.URI, true, null),
    GRANTTYPE("grant_type", ParameterType.STRING, true, "authorization_code"),
    CODE("code", ParameterType.STRING, true, null),
    CODE_VERIFIER("code_verifier", ParameterType.STRING, true, null);

    HTTPPostRequestParameter(String inName, ParameterType inType, boolean isRequired, String inRegex){
        this.name = inName;
        this.type = inType;
        this.required = isRequired;
        this.regex = inRegex;
    }

    final String name;
    final String regex;
    final ParameterType type;
    boolean required;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ParameterType getType() {
        return this.type;
    }

    @Override
    public boolean isRequired() {
        return this.required;
    }

    @Override
    public String getRegex() {
        return this.regex;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public static HTTPPostRequestParameter getParameterByName(String inName){
        for (HTTPPostRequestParameter parameter: values()){
            if (parameter.getName().equals(inName)){
                return parameter;
            }
        }
        return null;
    }

    public static List<HTTPPostRequestParameter> getAllParameters() {
        return Arrays.asList(values());
    }
}
