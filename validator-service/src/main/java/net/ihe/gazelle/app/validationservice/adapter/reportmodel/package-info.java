/**
 * Model classes for the validation result.
 *
 * @author <a href="abderrazek.boufahja@ihe-europe.net">Abderrazek Boufahja</a>
 */
package net.ihe.gazelle.app.validationservice.adapter.reportmodel;
