package net.ihe.gazelle.app.iti71validator.business;

public enum JWTParameterKey {

    ISSUER("iss", true),
    SUBJECT("sub", true),
    AUDIENCE("aud", true),
    EXPIRATION("exp", true),
    NOTBEFORE("nbf", true),
    ISSUEDAT("iat", true),
    CLIENTID("client_id", true),
    SCOPE("scope", true),
    JWTID("jti", true),
    EXTENSION("extensions", false),
    IHEIUA("ihe_iua", false),
    SUBJECTNAME("subject_name", false),
    PERSONID("person_id", false),
    SUBJECTROLE("subject_role", false),
    PURPOSEOFUSE("purpose_of_use", false),
    SYSTEM("system", false),
    CODE("code", false),
    CHEPR("ch_epr", false),
    USERID("user_id", false),
    USERIDQUALIFIER("user_id_qualifier", false),
    CHGROUP("ch_group", false),
    CHGROUPNAME("name", false),
    CHGROUPID("id", false),
    CHDELEGATION("ch_delegation", false),
    PRINCIPAL("principal", false),
    PRINCIPALID("principal_id", false);

    final String key;
    boolean required;

    JWTParameterKey(String key, boolean required) {
        this.key = key;
        this.required = required;
    }

    public String getKey() { return this.key; }

    public boolean isRequired() {
        return false;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public static JWTParameterKey getParameterByKey(String inKey){
        for (JWTParameterKey parameter: values()){
            if (parameter.getKey().equals(inKey)){
                return parameter;
            }
        }
        return null;
    }
}
