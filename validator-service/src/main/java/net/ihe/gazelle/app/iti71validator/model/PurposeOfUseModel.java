package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class PurposeOfUseModel {

    private String system;
    private String code;

    public PurposeOfUseModel(String system, String code) {
        this.system = system;
        this.code = code;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<JWTParameterValue, String> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, String> allParam = new TreeMap<>();
        if (this.system != null && this.code != null) {
            allParam.put(JWTParameterValue.PURPOSEOFUSESYSTEM, system);
            allParam.put(JWTParameterValue.PURPOSEOFUSECODE, code);
            return allParam;
        }
        return Collections.emptyMap();
    }
}
