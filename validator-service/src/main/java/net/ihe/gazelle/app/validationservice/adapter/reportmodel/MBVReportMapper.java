package net.ihe.gazelle.app.validationservice.adapter.reportmodel;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationCounters;
import net.ihe.gazelle.modelapi.validationreportmodel.business.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Mapper that transform a {@link ValidationReport} to a {@link DetailedResult}.
 */
public class MBVReportMapper {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(MBVReportMapper.class);

    /**
     * Default constructor for the class.
     */
    private MBVReportMapper() {
        //Empty constructor
    }

    /**
     * Map {@link ValidationReport} info to a {@link DetailedResult}
     *
     * @param validationReport report to map
     * @return the mapped {@link DetailedResult}
     */
    public static DetailedResult map(ValidationReport validationReport) {
        DetailedResult detailedResult = new DetailedResult();

        mapValidationOverview(validationReport.getValidationOverview(), detailedResult);
        MDAValidation mdaValidation = new MDAValidation();
        mdaValidation.setValidationCounters(new net.ihe.gazelle.app.validationservice.adapter.reportmodel.ValidationCounters());
        detailedResult.setMDAValidation(mdaValidation);
        mdaValidation.setResult(validationReport.getValidationOverallResult().name());

        if (!validationReport.getReports().isEmpty()) {
            validationReport.getReports().stream().forEach(validationSubReport -> mapValidationSubReport(validationSubReport, detailedResult));
        }

        return detailedResult;
    }

    /**
     * Map {@link ValidationReport#getValidationOverview()} )} information
     *
     * @param validationOverview to map
     * @param detailedResult     target report
     */
    private static void mapValidationOverview(ValidationOverview validationOverview, DetailedResult detailedResult) {
        if (validationOverview == null) {
            return;
        }
        detailedResult.setValidationResultsOverview(new ValidationResultsOverview());
        mapValidationDateTime(validationOverview.getValidationDateTime(), detailedResult);
        detailedResult.getValidationResultsOverview().setValidationServiceName(validationOverview.getValidationServiceName());
        detailedResult.getValidationResultsOverview().setValidationServiceVersion(validationOverview.getValidationServiceVersion());
        mapValidationOverallResult(validationOverview.getValidationOverallResult(), detailedResult);
    }

    /**
     * Map validationDateTime.
     *
     * @param validationDateTime date to map
     * @param detailedResult     target report
     */
    private static void mapValidationDateTime(Date validationDateTime, DetailedResult detailedResult) {
        if (validationDateTime == null) {
            return;
        }
        detailedResult.getValidationResultsOverview().setValidationDate(new SimpleDateFormat("yyyy-MM-dd").format(validationDateTime));
        detailedResult.getValidationResultsOverview().setValidationTime(new SimpleDateFormat("HH:mm:ss").format(validationDateTime));
    }

    /**
     * Map validationOverallResult.
     *
     * @param validationTestResult result to map
     * @param detailedResult       target report
     */
    private static void mapValidationOverallResult(ValidationTestResult validationTestResult, DetailedResult detailedResult) {
        if (validationTestResult == null) {
            return;
        }
        switch (validationTestResult) {
            case PASSED:
                detailedResult.getValidationResultsOverview().setValidationTestResult("PASSED");
                break;
            case FAILED:
                detailedResult.getValidationResultsOverview().setValidationTestResult("FAILED");
                break;
            case UNDEFINED:
                detailedResult.getValidationResultsOverview().setValidationTestResult("UNDEFINED");
                break;
        }
    }

    /**
     * Map validation sub-report.
     *
     * @param validationSubReport sub-report to map
     * @param detailedResult      target report
     */
    private static void mapValidationSubReport(ValidationSubReport validationSubReport, DetailedResult detailedResult) {
        if (validationSubReport == null) {
            return;
        }
        MDAValidation mdaValidation = detailedResult.getMDAValidation();

        mapConstraints(validationSubReport.getConstraints(), mdaValidation);
        mapMDACounters(validationSubReport.getSubCounters(), mdaValidation.getValidationCounters());
    }

    /**
     * Map a list of ConstraintValidation.
     *
     * @param constraintValidations list of constraints to map
     * @param mdaValidation         target sub-report
     */
    private static void mapConstraints(List<ConstraintValidation> constraintValidations, MDAValidation mdaValidation) {
        if (mdaValidation.warningOrErrorOrNote == null) {
            mdaValidation.warningOrErrorOrNote = new ArrayList<>();
        }
        for (ConstraintValidation constraintValidation : constraintValidations) {
            mdaValidation.warningOrErrorOrNote.add(getNotificationFromConstraint(constraintValidation));
        }
    }

    /**
     * Get a {@link Notification} of the right type depending on the constraint to map.
     *
     * @param constraintValidation constraint to map.
     * @return the {@link Notification}
     */
    private static Notification getNotificationFromConstraint(ConstraintValidation constraintValidation) {
        if (constraintValidation == null) {
            return null;
        }
        switch (constraintValidation.getSeverity()) {
            case INFO:
                return constraintValidation.getTestResult().equals(ValidationTestResult.FAILED) ? mapInfo(constraintValidation) :
                        mapNote(constraintValidation);
            case WARNING:
                return mapWarning(constraintValidation);
            case ERROR:
                return mapError(constraintValidation);
            default:
                return mapNote(constraintValidation);
        }
    }

    /**
     * Map the constraint to an {@link Error}
     *
     * @param constraintValidation constraint to map.
     * @return the mapped {@link Error}
     */
    private static Error mapError(ConstraintValidation constraintValidation) {
        Error error = new Error();
        mapNotification(constraintValidation, error);
        return error;
    }

    /**
     * Map the constraint to an {@link Warning}
     *
     * @param constraintValidation constraint to map.
     * @return the mapped {@link Warning}
     */
    private static Warning mapWarning(ConstraintValidation constraintValidation) {
        Warning warning = new Warning();
        mapNotification(constraintValidation, warning);
        return warning;
    }

    /**
     * Map the constraint to an {@link Info}
     *
     * @param constraintValidation constraint to map.
     * @return the mapped {@link Info}
     */
    private static Info mapInfo(ConstraintValidation constraintValidation) {
        Info info = new Info();
        mapNotification(constraintValidation, info);
        return info;
    }

    /**
     * Map the constraint to an {@link Note}
     *
     * @param constraintValidation constraint to map.
     * @return the mapped {@link Note}
     */
    private static Note mapNote(ConstraintValidation constraintValidation) {
        Note note = new Note();
        mapNotification(constraintValidation, note);
        return note;
    }

    /**
     * Map the constraint to a {@link Notification}
     *
     * @param constraintValidation constraint to map.
     * @param notification         notification to map into.
     */
    private static void mapNotification(ConstraintValidation constraintValidation, Notification notification) {
        notification.setDescription(constraintValidation.getConstraintDescription());
        notification.setLocation(constraintValidation.getLocationInValidatedObject());
        notification.setTest(constraintValidation.getConstraintType());

        for (String assertionId : constraintValidation.getAssertionIDs()) {
            notification.getAssertions().add(new Assertion("", assertionId));
        }
    }

    /**
     * Map sub-reports counters.
     *
     * @param validationCounters counter to map.
     * @param mdaCounters        target counter.
     */
    private static void mapMDACounters(ValidationCounters validationCounters,
                                       net.ihe.gazelle.app.validationservice.adapter.reportmodel.ValidationCounters mdaCounters) {
        mdaCounters.setNrOfChecks(BigInteger.valueOf(validationCounters.getNumberOfConstraints()));
        mdaCounters.setNrOfValidationErrors(BigInteger.valueOf(validationCounters.getNumberOfErrors()));
        mdaCounters.setNrOfValidationWarnings(BigInteger.valueOf(validationCounters.getNumberOfWarnings()));
        mdaCounters.setNrOfValidationInfos(BigInteger.valueOf(validationCounters.getFailedWithInfoNumber()));
    }

    /**
     * Transform a {@link DetailedResult} to an XML String.
     *
     * @param detailedResult report to transform to String.
     * @return the literal value of the report.
     */
    public static String getDetailedResultAsString(DetailedResult detailedResult) {
        if (detailedResult != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.app.validationservice.adapter.reportmodel");
                Marshaller m = jc.createMarshaller();
                m.marshal(detailedResult, baos);
            } catch (JAXBException e) {
                LOGGER.error(e, "Cannot marshal Detailed Result in String !");
            }
            return baos.toString(StandardCharsets.UTF_8);
        } else {
            return null;
        }
    }
}
