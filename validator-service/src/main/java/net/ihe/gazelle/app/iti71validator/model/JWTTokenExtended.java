package net.ihe.gazelle.app.iti71validator.model;

import com.fasterxml.jackson.databind.JsonNode;
import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Map;
import java.util.TreeMap;

public class JWTTokenExtended {

    private String issuer;
    private String subject;
    private String audience;
    private String expiration;
    private String notBefore;
    private String issuedAt;
    private String clientId;
    private String scope;
    private String jwtId;
    private JWTExtension extention;

    public JWTTokenExtended() {
    }

    public JWTTokenExtended(JWTTokenExtendedBuilder builder) {
        this.issuer = builder.issuer;
        this.subject = builder.subject;
        this.audience = builder.audience;
        this.expiration = builder.expiration;
        this.notBefore = builder.notBefore;
        this.issuedAt = builder.issuedAt;
        this.clientId = builder.clientId;
        this.scope = builder.scope;
        this.jwtId = builder.jwtId;
        this.extention = builder.extention;
    }

    public static JWTTokenExtendedBuilder builder() {
        return new JWTTokenExtendedBuilder();
    }

    public static class JWTTokenExtendedBuilder {
        private String issuer;
        private String subject;
        private String audience;
        private String expiration;
        private String notBefore;
        private String issuedAt;
        private String clientId;
        private String scope;
        private String jwtId;
        private JWTExtension extention;

        public JWTTokenExtendedBuilder withIssuer(final JsonNode issuer) {
            if (issuer == null) {
                this.issuer = null;
                return this;
            }
            this.issuer = issuer.textValue();
            return this;
        }

        public JWTTokenExtendedBuilder withSubject(final JsonNode subject) {
            if (subject == null) {
                this.subject = null;
                return this;
            }
            this.subject = subject.textValue();
            return this;
        }

        public JWTTokenExtendedBuilder withAudience(final JsonNode audience) {
            if (audience == null) {
                this.audience = null;
                return this;
            }
            this.audience = audience.textValue();
            return this;
        }

        public JWTTokenExtendedBuilder withExpiration(final JsonNode expiration) {
            if (expiration == null) {
                this.expiration = null;
                return this;
            }
            this.expiration = String.valueOf(expiration.longValue());
            return this;
        }

        public JWTTokenExtendedBuilder withNotBefore(final JsonNode notBefore) {
            if (notBefore == null) {
                this.notBefore = null;
                return this;
            }
            this.notBefore = String.valueOf(notBefore.longValue());
            return this;
        }

        public JWTTokenExtendedBuilder withIssuedAt(final JsonNode issuedAt) {
            if (issuedAt == null) {
                this.issuedAt = null;
                return this;
            }
            this.issuedAt = String.valueOf(issuedAt.longValue());
            return this;
        }

        public JWTTokenExtendedBuilder withClientId(final JsonNode clientId) {
            if (clientId == null) {
                this.clientId = null;
                return this;
            }
            this.clientId = clientId.textValue();
            return this;
        }

        public JWTTokenExtendedBuilder withScope(final JsonNode scope) {
            if (scope == null) {
                this.scope = null;
                return this;
            }
            this.scope = scope.textValue();
            return this;
        }

        public JWTTokenExtendedBuilder withJwtId(final JsonNode jwtId) {
            if (jwtId == null) {
                this.jwtId = null;
                return this;
            }
            this.jwtId = jwtId.textValue();
            return this;
        }

        public JWTTokenExtendedBuilder withExtension(final JWTExtension extention) {
            this.extention = extention;
            return this;
        }

        public JWTTokenExtended build() {
            return new JWTTokenExtended(this);
        }
    }

    public String getIssuer() {
        return issuer;
    }

    public String getSubject() {
        return subject;
    }

    public String getAudience() {
        return audience;
    }

    public String getExpiration() { return expiration; }

    public String getNotBefore() {
        return notBefore;
    }

    public String getIssuedAt() {
        return issuedAt;
    }

    public String getClientId() {
        return clientId;
    }

    public String getScope() {
        return scope;
    }

    public String getJwtId() {
        return jwtId;
    }

    public JWTExtension getExtention() {
        return extention;
    }

    public Map<JWTParameterValue, Object> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, Object> allParam = new TreeMap<>();
        if (this.issuer != null) {
            allParam.put(JWTParameterValue.ISSUER, issuer);
        }
        if (this.subject != null) {
            allParam.put(JWTParameterValue.SUBJECT, subject);
        }
        if (this.audience != null) {
            allParam.put(JWTParameterValue.AUDIENCE, audience);
        }
        if (this.expiration != null) {
            allParam.put(JWTParameterValue.EXPIRATION, expiration);
        }
        if (this.notBefore != null) {
            allParam.put(JWTParameterValue.NOTBEFORE, notBefore);
        }
        if (this.issuedAt != null) {
            allParam.put(JWTParameterValue.ISSUEDAT, issuedAt);
        }
        if (this.clientId != null) {
            allParam.put(JWTParameterValue.CLIENTID, clientId);
        }
        if (this.scope != null) {
            allParam.put(JWTParameterValue.SCOPE, scope);
        }
        if (this.jwtId != null) {
            allParam.put(JWTParameterValue.JWTID, jwtId);
        }
        if (this.extention != null) {
            allParam.putAll(extention.getNonNullParamAsJWTParameterValue());
        }

        return allParam;
    }
}